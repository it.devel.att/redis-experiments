package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	rdb := redis.NewFailoverClusterClient(&redis.FailoverOptions{
		MasterName:    "redis-test",
		Password:      "foobared",
		SentinelAddrs: []string{"127.0.0.1:36379", "127.0.0.1:46379", "127.0.0.1:56379"},
	})
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	defer cancel()

	if err := rdb.Ping(ctx).Err(); err != nil {
		log.Fatal(err)
	}
	wg.Add(1)

	go func() {
		defer wg.Done()
		const pubsubChannel = "sub:chan"

		counter := 0
		pubsub := rdb.Subscribe(ctx, pubsubChannel).Channel()
		for {
			select {
			case m, ok := <- pubsub:
				if !ok {
					continue
				}
				log.Println(m.String())
			case <-ctx.Done():
				fmt.Println("Stop goroutine")
				return
			default:
				counter++
				if err := rdb.Incr(ctx, "key:for:inc").Err(); err != nil {
					log.Println(err)
				}
				if err := rdb.Set(ctx, fmt.Sprintf("key:%v", counter), counter, time.Hour).Err(); err != nil {
					log.Println(err)
				}
				if err := rdb.Publish(ctx, pubsubChannel, counter).Err(); err != nil {
					log.Println(err)
				}
				time.Sleep(time.Millisecond * 100)
			}
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	<-sigChan

	cancel()
	wg.Wait()
}
