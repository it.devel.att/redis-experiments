## Simple master slave example
[Thanks to this tutorial](https://rtfm.co.ua/redis-replikaciya-chast-1-obzor-replication-vs-sharding-sentinel-vs-cluster-topologiya-redis/)

Run docker-compose for simple master-slave example
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker-compose -f dc.master-slave.yml up
```

In new master terminal
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-master redis-cli -a foobared
```
In master terminal write
```console
127.0.0.1:6379> info replication
# Replication
role:master
connected_slaves:1
slave0:ip=172.29.0.3,port=6379,state=online,offset=196,lag=1
master_replid:0718137d1bb3a11ed6ef1d2acac10dbd03f62748
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:210
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:210
```

Open new slave terminal
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-slave redis-cli -a foobared
```
In terminal write
```console
127.0.0.1:6379> info replication
# Replication
role:slave
master_host:redis-master
master_port:6379
master_link_status:up
master_last_io_seconds_ago:6
master_sync_in_progress:0
slave_repl_offset:336
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:0718137d1bb3a11ed6ef1d2acac10dbd03f62748
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:336
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:336
```
Check data synchronization:

On master terminal:
```console
127.0.0.1:6379> SET test hello_world
OK
```
On slave terminal:
```console
127.0.0.1:6379> GET test
"hello_world"
```
Replication works!

## Redis sentinel example
> https://github.com/redis/redis/issues/8172
```
chmod -R 0777 configs/sentinel/master.conf
```

```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker-compose -f dc.redis-sentinel.yml up
```
Check sentinel status:
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-master-sentinel redis-cli -p 26379 info sentinel
# Sentinel
sentinel_masters:1
sentinel_tilt:0
sentinel_running_scripts:0
sentinel_scripts_queue_length:0
sentinel_simulate_failure_flags:0
master0:name=redis-test,status=ok,address=192.168.192.6:6379,slaves=2,sentinels=3
```
> Try few times **info sentinel** it must became **slaves=2,sentinels=3**


Write to master
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-master redis-cli -a foobared SET hello world
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
OK
```
Read from slaves:
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-slave-1 redis-cli -a foobared GET hello
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
"world"
```
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-slave-2 redis-cli -a foobared GET hello
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
"world"
```
Check that slaves cannot write:
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-slave-1 redis-cli -a foobared SET hello world
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
(error) READONLY You can't write against a read only replica.
```
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-slave-2 redis-cli -a foobared SET hello world
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
(error) READONLY You can't write against a read only replica.
```

#### Stop master for check reconfiguration
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-master redis-cli -a foobared DEBUG sleep 90
```

After this sentinels must re-configure slaves and one of them became new master:
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-slave-2 redis-cli -a foobared SET hello new_world
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
OK
```
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-slave-1 redis-cli -a foobared SET hello world
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
(error) READONLY You can't write against a read only replica.
```
For me **redis-slave-2** became new master

After master start be available he became slave of new master (cannot set values, and can only read):
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-master redis-cli -a foobared SET hello world
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
(error) READONLY You can't write against a read only replica.
```
```console
yaroslav@yaroslav:~/ForFun/redis-cluster$ docker exec -it redis-master redis-cli -a foobared GET hello
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
"new_world"
```